import Vue from 'vue'
import Vuex from 'vuex'
import createPersistedState from 'vuex-persistedstate'

Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    player_id_onesignal: '',
  },
  mutations: {
    SET_PLAYER_ID_ONESIGNAL(state, player_id){
      console.log('ACTIVE MUTATION with player Id', player_id);
      state.player_id_onesignal = player_id;
    },
  },
  actions: {
    savePlayerIdOneSignal({commit}, player_id_onesignal) {
      console.log('recive player_id_onesignal action', player_id_onesignal, 'and commit mutation');
      commit('SET_PLAYER_ID_ONESIGNAL', player_id_onesignal)
    },
  },
  modules: {
  },
  getters: {
    getPlayerId(state){
      console.log('ACTIVE GETTER', state.player_id_onesignal);
      return state.player_id_onesignal
    },
  },
  plugins: [
    createPersistedState({ storage: process.client && window.localStorage })
  ]
})
