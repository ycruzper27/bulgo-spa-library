import moment from 'moment'
import Vue from 'vue';
import Axios from './axios';
import VBreakpoint from './index';
import { EventBus, EventBusEvents } from './EventBus';
const filtersVue = {
    currency(value = '') {
        let val = (value/1).toFixed(0).replace('.', ',')
        return val.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ".") == '0' ? '' : val.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ".")
    },

    dateFormat(_date = new Date) {
        return moment(_date).format('DD/MM/YYYY hh:mm A')
    }
}

const registerFiltersAndHelpersVue = (vuetifyApp, registerPrototypesInstances = true, envData) => {
    // * Register Filters
    Object.keys(filtersVue).map(key => Vue.filter(key, filtersVue[key]));

    // * Add listener to refresh page, remove localstorage
    window.addEventListener('beforeunload', function() {
        localStorage.removeItem('@preload_app');
        localStorage.removeItem('@timeAppPreload');
    });

    // * register helper Vuetify breakpoints
    if(registerPrototypesInstances)
        Vue.prototype.$VBreakpoint = new VBreakpoint(vuetifyApp);
        // * Register Axios Helper
        Vue.prototype.$axios = new Axios(envData);
        // * Register global event Bus Events
        Vue.prototype.$EventBus = EventBus;
        Vue.prototype.$EventBusEvents = {
            ...EventBusEvents,
            ...envData.EventBusEvents
        };
}

export {
    filtersVue,
    registerFiltersAndHelpersVue
}