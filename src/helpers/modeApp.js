import BulgoLogo from './bulgoLogo';
import variables from '../assets/scss/variables';

export default class ModeApp {
    type_app = 'desktop'
    time_change_night = '20:00:00'
    time_change_day = '07:00:00'
    BulgoLogo = BulgoLogo('blue')
    // * Get styles preload app

    validateTimeApp(routeApp, timeAppRequire = 'day_app') {
        console.log('get preload app', localStorage.getItem('@preload_app')
            ,localStorage.getItem('@timeAppPreload'), timeAppRequire, routeApp);
        // * First ask if the preload app is same to app want load,
        // * Too, ask if time app preload is same to time app want load.
        if(
            localStorage.getItem('@preload_app') &&
            (localStorage.getItem('@preload_app') == routeApp) &&
            localStorage.getItem('@timeAppPreload') &&
            (localStorage.getItem('@timeAppPreload') == timeAppRequire)
        ) return

        else {
            this.toggleOverlayLoader(true);
            console.log('validate time app service for route...',routeApp, timeAppRequire);
            if(new RegExp(routeApp).test(window.location.pathname)) {
                setInterval(() => {
                    const timeNow = +new Date(),
                    time_change_day = +new Date(`${new Date().toDateString()} ${this.time_change_day}`),
                    time_change_night = +new Date(`${new Date().toDateString()} ${this.time_change_night}`)

                    if(!this.isMobile()) {
                        this.changeConfigDesktop()
                        this.updatePreloadApp(routeApp, timeAppRequire);
                    } else {
                        if(timeNow >= time_change_day && timeNow < time_change_night) {
                            if(this.isMobile() && this.type_app != 'day_app') {
                                this.changeConfigDay()
                                this.updatePreloadApp(routeApp, timeAppRequire);
                            }
                        } else {
                            if(this.isMobile() && this.type_app != 'night_app') {
                                this.changeConfigNight()
                                this.updatePreloadApp(routeApp, timeAppRequire);
                            }
                        }
                    }
                },1000)
            }
        }
    }

    changePropertyCss(payload) {
        if(process.browser) {
            document.documentElement.style.setProperty(payload.variable, variables[payload.value])
        }
    }

    isMobile() {
        if(process.browser) {
            if(/Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent)) {
                return true
            } else {
                return false
            }
        } else return false
    }

    updatePreloadApp(routeApp, timeApp){
        localStorage.setItem('@preload_app', routeApp);
        localStorage.setItem('@timeAppPreload', timeApp);
    }

    toggleOverlayLoader(value){
        // * Create Event
        const evt = document.createEvent("Event");
        evt.initEvent('toggleOverlayLoader',true,true);
        evt.valueOverlay = value;

        // ? Dispatch Event
        document.dispatchEvent(evt);
    }

    onOverlayLoader(callback){
        document.addEventListener("toggleOverlayLoader",callback,false);
    }

    getTimesApp() {
        const timeNow = +new Date(),
            time_change_day = +new Date(`${new Date().toDateString()} ${this.time_change_day}`),
            time_change_night = +new Date(`${new Date().toDateString()} ${this.time_change_night}`);

        let requireTime = null;


            if(!this.isMobile()) requireTime = 'desktop'
            else {
                if(timeNow >= time_change_day && timeNow < time_change_night)
                    requireTime = 'day_app'
                else requireTime = 'night_app'
            }

        return {
            timeNow,
            time_change_day,
            time_change_night,
            requireTime
        }
    }
}