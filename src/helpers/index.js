
import vuetify from '../plugins/vuetify';

export default class VBreakpoint {
    vuetifyB = vuetify;
    vfyBreakpoint = vuetify.framework.breakpoint;

    constructor(vuetifyApp){
        this.vuetifyB = vuetifyApp || vuetify;
        this.vfyBreakpoint = this.vuetifyB.breakpoint;
    }

    isDesktop() {
        return this.vfyBreakpoint.lgAndDown &&
            (this.vfyBreakpoint.name == 'md' || this.vfyBreakpoint.name == 'lg')
    }

    isTablet() {
        return this.vfyBreakpoint.mdAndDown && this.vfyBreakpoint.name == 'sm'
    }

    isPhone() {
        return this.vfyBreakpoint.smAndDown && this.vfyBreakpoint.name == 'xs'
    }
}