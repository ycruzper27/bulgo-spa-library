const fs = require('fs');
const variables = fs.readFileSync('./src/assets/scss/variables.scss', 'utf-8');
const varJS = fs.readFileSync('./src/assets/scss/variables.js', 'utf-8');

// * Return array with variables CSS
const formatArrayVarCSS = () => {
    const variablesFormatted = variables.replace(':root {', '').replace('}', '');
    const re = /--/g;
    const responseExec = re.exec(variablesFormatted).input;
    const splitExec = responseExec.split(';');
    return splitExec;
}

// * Return array with variables JS about CSS Variables
const formatVarJs = () => {
    let varJsFormatted = varJS.replace(`export default new class Variables {`, '');
    varJsFormatted = varJsFormatted.replace(`}`, '');

    const arrayReturn = [];
    const re = /=/g
    const responseExec = re.exec(varJsFormatted).input;
    const splitExec = responseExec.split(';');

    splitExec.forEach(item => {
        const splitItem = item.split('=');
        arrayReturn.push({
            jsDefault: item,
            keyName: splitItem[0],
            value: splitItem[1]
        })
    })
    return arrayReturn;
}

// * Method to change value in var CSS file
const findVarAndChangeValue = (varToChange, valueForChange) => {
    let valueCSSFind = {};

    // * First, find value to change this variable
    variablesArrayJS.forEach(item => {
        const expVar = new RegExp(valueForChange, 'g');
        if(expVar.test(item.keyName)) valueCSSFind = item;
    })

    // * After, find var CSS in the array
    variablesArrayCSS.forEach((item,index) => {
        let splitItem = item.split(':');
        let itemUpdated = '';
        const expVar = new RegExp(varToChange, 'g');
        if(expVar.test(item)) {
            splitItem[1] = `${valueCSSFind.value.replace(/'/g, '').replace(' ', '')};`;
            itemUpdated = splitItem.join(':');
            variablesArrayCSS[index] = itemUpdated;
        } else {
            if(splitItem[1]) {
                splitItem[1] = `${splitItem[1]};`
                variablesArrayCSS[index] = splitItem.join(':');
            }
        }
    })
}

// * Get variables in format array
let variablesArrayCSS = [];
let variablesArrayJS = [];

export default (variablesArrayToChange) => {

    variablesArrayCSS = formatArrayVarCSS(variables);
    variablesArrayJS = formatVarJs();

    // * Iterate all variables to change value, and after write new file
    variablesArrayToChange.forEach(item => {
        findVarAndChangeValue(item.variable, item.value)
    })

    const fileSCSSWrite = `
        :root {
            ${variablesArrayCSS.join('')}
        }
    `

    // TODO  Write File with all variables!
    fs.writeFileSync('./src/assets/scss/variables.scss', fileSCSSWrite);
    console.log('WRITE NEW SCSS FILE!');
}
