import axios from 'axios';

export default class Axios {
    baseURL = 'https://bulgo.com/v1/api';
    youtubeURL = 'https://www.googleapis.com/youtube/v3/search';
    // * 9 Api KEY for besting the search quote
    API_KEY_Youtube = [
        'AIzaSyCT6uAcivmXAUlWiiF0uxVDBEdvuvG8JTs',
        'AIzaSyCHjpRwrwEXenmbvqKhd8EpzM4JMuX-Rp8',
        'AIzaSyCe2w_naEeqtnvcQG_45HmKtnKI6GYtm_s',
        'AIzaSyBjd-j5aBnH2dj9b3Y4h7sTT4C_A4F02vc',
        'AIzaSyDVQkLJfrpnfaYZPtFhqlbqKV-glDVS_dU',
        'AIzaSyCRlwR1q5RnwfNomBaeqDjKMPP4GcGxm-s',
        'AIzaSyDdXz8z3YfsCm7_L4XYIj3_bBH_wqFbji0',
        'AIzaSyDujkuaFfUwR3KXduH97j4DGC8Na93sTxM',
        'AIzaSyBOQZ2hB0Ay95Ci4_J83uDXIekSZ5v9TlQ'
    ];

    https = axios;
    constructor(envData){

        this.baseURL = envData.VUE_APP_BASE_URL || this.baseURL;
        this.https = axios.create({
            baseURL: this.baseURL,
            timeout: 15000,
            // headers: {
            //     'Authorization': 'Bear '
            // }
        })
        console.log('get find best api key', localStorage.getItem('@findBestApi') , localStorage.getItem('@indexApi'));
        if(!localStorage.getItem('@findBestApi')) {
            this.findBestApiKeyYoutube()
        }
    }

    async get(endpoint, params = {}) {
        try {
            const resp = await this.https.get(endpoint, { params })
            return resp.data;
        } catch (error) {
            return this.handleError(error);
        }
    }

    async getYoutube(q = '', API_KEY = null) {
        try {
            const resp = await axios.get(this.youtubeURL, {
                params: {
                    part: 'snippet',
                    key: API_KEY || this.API_KEY_Youtube[+localStorage.getItem('@indexApi')],
                    maxResults: 7,
                    q
                }
            })
            return this.youtubeItemsFormatted(resp.data.items);
        } catch (error) {
            if(!API_KEY) {
                console.log('fail the request yotube', {
                    q
                }, 'try find the best api', localStorage.getItem('@indexApi'));
                // * Try find best api Youtube again
                await this.findBestApiKeyYoutube();
                // * If find best api and index, try again same query
                if(localStorage.getItem('@findBestApi') && localStorage.getItem('@indexApi')){
                    await this.getYoutube(q);
                }
            } else {
                return this.handleError(error);
            }
        }
    }
    youtubeItemsFormatted(youtubeItems = []) {
        const dataFormatted = [];
        youtubeItems.forEach((item) => {
            if(item.id.videoId) {
                dataFormatted.push({
                    idVideo: item.id.videoId,
                    ...item.snippet
                })
            }
        })
        return dataFormatted;
    }
    handleError(error) {
        throw {
            type: 'error',
            error: error.response.data || error,
            message: error.message || 'Error Undefined',
            code: error.response.status || 0
        }
    }

    async findBestApiKeyYoutube() {
        // * Clear index API for search
        localStorage.removeItem('@indexApi');
        console.log('testing youtube api key...');
        for (let i = 0; (i < this.API_KEY_Youtube.length && !localStorage.getItem('@indexApi')); i++) {
            const item = this.API_KEY_Youtube[i];
            try {
                // * If is success, the index not change
                await this.getYoutube('test', item);
                localStorage.setItem('@indexApi', i);
                // * Update store find best key
                localStorage.setItem('@findBestApi', true);
            } catch (error) {
                console.log('FALLO CON ESTE API', item, 'seguira buscando?');
            }
        }
        // * Finally for, if index is null, add value for default
        if(!localStorage.getItem('@indexApi')) {
            const randomIndex = Math.floor(Math.random() * this.API_KEY_Youtube.length-1) + 1;
            localStorage.setItem('@indexApi', randomIndex);
            // * Update store find best key false value
            localStorage.removeItem('@findBestApi');
        }

        console.log('finally index for youtube api key', localStorage.getItem('@indexApi'));
    }
}