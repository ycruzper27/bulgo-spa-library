
import BulgoLogoWhite from '../assets/icons/BULGO_icon_white.svg?inline';
import BulgoLogoGold from '../assets/icons/BULGO_icon_gold.svg?inline';
import BulgoLogoBlue from '../assets/icons/BULGO_icon_blue.svg?inline';

export default (color) => {
    if(color == 'white')
        return BulgoLogoWhite;
    else if(color == 'blue')
        return BulgoLogoBlue;
    else if(color == 'gold')
        return BulgoLogoGold;
    else return BulgoLogoWhite;
}