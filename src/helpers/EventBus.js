const EventBusEvents = {
    changeIconBulgo: 'changeIconBulgo',
    openMenuSearchResults: 'openMenuSearchResults',
}

const EventBus = new class EventBus {
    $emit(event, args) {
        // * Create Event
        const evt = document.createEvent("Event");
        evt.initEvent(event,true,true);
        evt[`args_${event}`] = args;

        // ? Dispatch Event
        document.dispatchEvent(evt);
    }

    $on(event, callback) {
        document.addEventListener(event,(e) => callback(e[`args_${event}`]),false);
    }
}

export {
    EventBus,
    EventBusEvents
}