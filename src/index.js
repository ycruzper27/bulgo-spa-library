import storeBulgo from './store/index'
import './assets/scss/global.scss'
import variables from './assets/scss/variables'
import VBreakpoint from './helpers/index';
import BulgoLogo from './helpers/bulgoLogo';
import { filtersVue, registerFiltersAndHelpersVue } from './helpers/filters';
import ModeApp from './helpers/modeApp'
import { EventBus, EventBusEvents } from './helpers/EventBus';

// * Components
import LinkDevice from './components/LinkDevice.vue';
import LoaderBulgo from './components/LoaderBulgo.vue';
import Autocomplete from './components/Autocomplete.vue';
import SlideMultiple from './components/SlideMultiple.vue';
import VueQRCodeComponent from 'vue-qrcode-component'

export {
    storeBulgo,
    variables,
    VBreakpoint,
    BulgoLogo,
    registerFiltersAndHelpersVue,
    filtersVue,
    ModeApp,
    EventBus,
    EventBusEvents,
    // * Components
    LoaderBulgo,
    LinkDevice,
    Autocomplete,
    SlideMultiple,
    VueQRCodeComponent
}