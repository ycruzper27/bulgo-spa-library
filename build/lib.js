const fs = require('fs')
const packageJson = fs.readFileSync('./package.json')

// * get version package and split for ".", exp: ["0", "1", "1"];
const splitVersion = JSON.parse(packageJson).version.split('.');
let newVersion = 0, updateNumberVersion = 0;
const MAX_UPDATE_VERSION_NUMBER = 150
// * Get last numbers and update +1 to number MAX_UPDATE_VERSION_NUMBER, here update second number to array

// ? If second number array is minus MAX_UPDATE_VERSION_NUMBER, ask if third number array is upgradeable or not
if(+splitVersion[1] <= MAX_UPDATE_VERSION_NUMBER) {
    // * If last number is minus MAX_UPDATE_VERSION_NUMBER, update +1
    if(+splitVersion[splitVersion.length-1] < MAX_UPDATE_VERSION_NUMBER) {
        updateNumberVersion = +splitVersion[splitVersion.length-1] + 1;
        splitVersion[splitVersion.length-1] = updateNumberVersion.toString();
        newVersion = splitVersion.join('.');
    } else {
        // * Else if second number is minus MAX_UPDATE_VERSION_NUMBER, then, update second number and reset third number
        if(+splitVersion[1] < MAX_UPDATE_VERSION_NUMBER) {
            updateNumberVersion = +splitVersion[1] + 1;
            splitVersion[1] = updateNumberVersion.toString();
            // * reset third number
            splitVersion[splitVersion.length-1] = "0";
            newVersion = splitVersion.join('.');
        } else {
            // * Else second number is same MAX_UPDATE_VERSION_NUMBER, reset third and second, AND update first number
            updateNumberVersion = +splitVersion[0] + 1;
            splitVersion[0] = updateNumberVersion.toString();
            // * reset third and second number
            splitVersion[splitVersion.length-1] = "0";
            splitVersion[1] = "0";
            newVersion = splitVersion.join('.');
        }

    }
} else {
    // * If third number is minus MAX_UPDATE_VERSION_NUMBER, update +1
    if(+splitVersion[splitVersion.length-1] < MAX_UPDATE_VERSION_NUMBER) {
        updateNumberVersion = +splitVersion[splitVersion.length-1] + 1;
        splitVersion[splitVersion.length-1] = updateNumberVersion.toString();
        newVersion = splitVersion.join('.');
    } else {
        // ? Else second number is plus MAX_UPDATE_VERSION_NUMBER, reset third and second, AND update first number
        updateNumberVersion = +splitVersion[0] + 1;
        splitVersion[0] = updateNumberVersion.toString();
        // * reset third and second number
        splitVersion[splitVersion.length-1] = "0";
        splitVersion[1] = "0";
        newVersion = splitVersion.join('.');
    }
}

/*
    TODO
    * After All Process!!
    * Write file packageJson with the new version!!!
*/

const newPackageJson = JSON.parse(packageJson);
newPackageJson.version = newVersion;

fs.writeFileSync('./package.json', JSON.stringify(newPackageJson, null, 4));


console.log('new Version package --->', newPackageJson.version);