const fs = require('fs');
// const util = require('util');
const variables = fs.readFileSync('./src/assets/scss/global.scss', 'utf-8');

const getVariablesFormat = (arraySplitExec) => {
    const variablesScss = [];
    arraySplitExec.forEach(item => {
        // * First clear string
        const itemClear = item.replace(/\n/g, '');
        // * After, split for [key, value]
        const splitItem = itemClear.split(':');
                                // * Delete variables comment
        if(/var/g.test(item) && !/\//g.test(item)) {
            variablesScss.push(`${splitItem[0].replace(/-/g, '_')} = '${splitItem[1].replace(/;/g, '')}'`);
        }
    })
    return variablesScss
}

const formatArrayVarScss = (data) => {

    const re = /\$/g;
    const responseExec = re.exec(data).input;
    const splitExec = responseExec.split('$');
    const variablesScss = getVariablesFormat(splitExec);
    return variablesScss;
}

// * Get variables in format array
const variablesArrayScss = formatArrayVarScss(variables);
let fileJsVariables = '';

// * Iterate all variables, and added in fileJs
variablesArrayScss.forEach(item => {
    fileJsVariables += `${item};\n`
})


// * Create class JS and add all variables

fileJsVariables = `
 export default new class Variables {
    ${fileJsVariables}
}
`

// TODO  Write File with all variables!
fs.writeFileSync('./src/assets/scss/variables.js', fileJsVariables);
