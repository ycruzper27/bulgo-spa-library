const fs = require('fs');
// const util = require('util');
const variables = fs.readFileSync('./src/assets/scss/variables.scss', 'utf-8');

const getVariablesFormat = (arraySplitExec) => {
    const variablesCSS = [];
    arraySplitExec.forEach(item => {
        // * First clear string
        const itemClear = item.replace(/\n/g, '');
        if(itemClear.length > 0) {
            // * After, split for [key, value]
            const splitItem = itemClear.split(':');
            variablesCSS.push(`${splitItem[0].replace('--', '').replace(/-/g, '_')} = '${splitItem[1].replace(/;/g, '')}'`);
        }
    })
    return variablesCSS
}

const formatArrayVarCSS = (data) => {
    let variablesFormatted = data.replace(':root {', '');
    variablesFormatted = variablesFormatted.replace('}', '');
    const re = /--/g;
    const responseExec = re.exec(variablesFormatted).input;
    const splitExec = responseExec.split(';');
    const variablesCSS = getVariablesFormat(splitExec);
    return variablesCSS;
}

// * Get variables in format array
const variablesArrayCSS = formatArrayVarCSS(variables);
let fileJsVariables = '';

// * Iterate all variables, and added in fileJs
variablesArrayCSS.forEach(item => {
    fileJsVariables += `${item};\n`
})


// * Create class JS and add all variables

fileJsVariables = `
 export default new class Variables {
    ${fileJsVariables}
}
`

// TODO  Write File with all variables!
fs.writeFileSync('./src/assets/scss/variables.js', fileJsVariables);
console.log('WRITE NEW JS FILE!');
